Webpack-component-builder
Version 0.0.1

To run this lib builder you need to add in your package.json following scripts in script section:

//package.json

...
"scripts": {
    ...
    "lib:prod": "webpack --config ./node_modules/webpack-component-builder/config/npm.build.config.js --progress -p",
    "lib": "webpack --config ./node_modules/webpack-component-builder/config/npm.build.config.js --progress",
    ...
}
...